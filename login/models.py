from django.db import models

# Create your models here.
class Subscriber(models.Model):
    nama = models.CharField(max_length=250, primary_key=True)
    email = models.EmailField(null=True, blank=True, unique=True)
    password = models.CharField(max_length=20)