from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import register
from .models import Subscriber

# Create your tests here.
class LoginPageTest(TestCase):

    def test_login_page_url_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_login_page_url_using_login_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_login_page_templates_valid(self):
        request = HttpRequest()
        response = register(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Register', html_response)

    def test_subscriber_registered(self):
        dummy_subscriber = Subscriber.objects.create(
            nama = "Prabiwo",
            email = "prabiwo@adirmakmul.com",
            password = "plesidenplesidenan"
        )
        counter = Subscriber.objects.all().count()
        self.assertEqual(counter, 1)
