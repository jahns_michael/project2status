from django.urls import path
from .views import register, check_email, create, show, delete

urlpatterns = [
    path('', register, name='register'),
    path('check-email/', check_email , name='check_email'),
    path('create/', create , name='create'),
    path('show/', show, name='show'),
    path('delete/', delete, name='delete'),
]