from django.shortcuts import render
from .models import Subscriber
from .forms import Subscriber_Form
from django.http import HttpResponseRedirect, JsonResponse

# Create your views here.

def register(request):
    subs_form = Subscriber_Form()
    db = Subscriber.objects.all()
    content = {'form': subs_form, 'data': db}
    return render(request, 'register.html', content)

def create(request):
    if request.method == "POST":
        nama = request.POST["nama"]
        email = request.POST["email"]
        password = request.POST["password"]

        new_subs = Subscriber.objects.create(
            nama = nama,
            email = email,
            password = password
        )
        new_subs.save()
        return HttpResponseRedirect("/register/")


def show(request):
    db = Subscriber.objects.all()
    response_data = []
    for data in db:
        response_obj = dict(nama=data.nama, email=data.email)
        response_data.append(response_obj)
    return JsonResponse(response_data, safe=False)


def check_email(request):
    if request.method == "GET":
        return HttpResponseRedirect("/register/")
    else:
        email_check = request.POST['email']
        response_data = {}
        response_data['is_exist'] = Subscriber.objects.filter(email= email_check).exists()
        response_data['is_valid'] = ('@' in email_check) and ('.' in email_check)
    return JsonResponse(response_data)

def delete(request):
    if request.method == "POST":
        deleted_email = request.POST['deleted_email']
        query = Subscriber.objects.get(email= deleted_email)
        query.delete()
    return HttpResponseRedirect("/register/")
