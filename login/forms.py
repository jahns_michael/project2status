from django import forms
from .models import Subscriber
class Subscriber_Form(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = ['nama', 'email', 'password']
        widgets = {
            'nama' : forms.TextInput(attrs={
                'placeholder': 'Your Name',
                'class':'form-content',
                'name' : 'nama',
                'id': 'nama',
                'required': 'True'
            }),
            'email' : forms.TextInput(attrs={
                'placeholder': 'Your Email',
                'class':'form-content',
                'name' : 'email',
                'id': 'email',
                'onkeyup': 'check_email(this);',
                'required': 'True'
            }),
            'password' : forms.PasswordInput(attrs={
                'class':'form-content',
                'name' : 'password',
                'id': 'password',
                'type' : 'password',
                'required': 'True'
            })
        }

