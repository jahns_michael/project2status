function check_email(element) {
    current_checked = $(element).val();

    $.ajax({
		url : "check-email/",
		data : {
			"csrfmiddlewaretoken" : $(element).siblings("input[name='csrfmiddlewaretoken']" ).val(),
			"email": current_checked
		},
		method: "POST",
		dataType : "json",
		success : function (data) {
			//console.log(data);
			if (data.is_valid) {
                $("#message").html("");
                if (data.is_exist) {
                    $("#message").html("Email udh ada cuy");
                    DisabledButton();
                } else {
                    $("#message").html("");
                    SubmitButton();
                }
            } else {
                $("#message").html("Bukan format email cuy");
                DisabledButton();
            }
		}
	});
}

$(document).on("submit", "#new-subscriber", function(e){
    e.preventDefault();

    $.ajax({
        url : "create/",
        type: "POST",
        data : {
            nama:$("#nama").val(),
            email:$("#email").val(),
            password:$("#password").val(),
            csrfmiddlewaretoken:$("input[name=csrfmiddlewaretoken]").val(),
        },
        success : function() {
            $("#nama").val("");
            $("#email").val("");
            $("#password").val("");
            alert("Data berhasil disimpan.");
        }
    });  

});


$("a").click(function(){
    var id = $(this).attr("id");
    if (id == "show-data") {
        
        $.ajax({
            type : "GET",
            url : "show/",
            dataType: "json",
            success : function(data) {
                var innerHTML = "";
                var classHTML = "list-group-item";
                var openTag = "<li class=" + classHTML;
                for (var i = 0; i < data.length; i++) {
                    // deleteButton = "<button type='button' class='btn btn-danger col-3' id=btn" + data[i].email +  ">Delete</button>"
                    innerHTML += openTag + " id=" + data[i].nama + " > Nama : " + data[i].nama + " | Email : " + data[i].email + "</li>";
                    // innerHTML += "<div class='d-flex flex-row'>" + preHTML + "</div>";
                }
                $(".list-group").html(innerHTML);

                var buttonGroup = "";
                for (var i = 0; i < data.length; i++) {
                    buttonGroup += "<button type='button' class='btn btn-danger' id=" + data[i].email +  ">Delete</button>";
                }
                $("#delete-button").html(buttonGroup);
                $("#subs-data-list").attr("style", "display: block");
                $("a").html("Hide data");
                $("a").attr("id", "hide-data");

                $(".btn-danger").click(function() {
                    current_deleted = $(this).attr("id");
                    var result = confirm("Are you sure you want to delete this subscriber?");
                
                    if (result) {
                        $.ajax({
                            type : "POST",
                            url : "delete/",
                            data : {
                                deleted_email : current_deleted,
                                csrfmiddlewaretoken:$("input[name=csrfmiddlewaretoken]").val()
                            },
                            success : function() {
                                alert("Delete Success!!")
                                $("#subs-data-list").attr("style", "display: none!important");
                                $("a").html("Show data");
                                $("a").attr("id", "show-data");
                            }
                        });
                    } else {
                
                    }
                
                });
            }
        });

    } else {
        $("#subs-data-list").attr("style", "display: none!important");
        $(this).html("Show data");
        $(this).attr("id", "show-data");
    }
});


function SubmitButton() {
    $("#submit-btn").prop("disabled", false);
}

function DisabledButton() {
    $("#submit-btn").prop("disabled", true);
}




// function functionConfirm(msg, myYes, myNo, cancel) {
//     var confirmBox = $("#confirm");
//     confirmBox.find(".message").text(msg);
//     confirmBox.find(".yes,.no,.cancel").unbind().click(function() {
//        confirmBox.hide();
//     });
//     confirmBox.find(".yes").click(myYes);
//     confirmBox.find(".no").click(myNo);
//     confirmBox.find(".no").click(cancel);
//     confirmBox.show();
//  }
 
 