from selenium import webdriver
from django.contrib.staticfiles.testing import LiveServerTestCase
from django.urls import reverse
from status_page.models import Status
import time
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
import time
# from selenium.webdriver.support.ui import WebDriverWait


class TestStatusPage(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(TestStatusPage, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(TestStatusPage, self).tearDown()

    ## test for story_7

    def test_status_created(self):
        import random

        chrome = self.browser
        chrome.get('http://127.0.0.1:8000/')

        time.sleep(4)

        status_sentence = chrome.find_element_by_name("status_sentence")
        submit_btn = chrome.find_element_by_id("submit-btn")

        random_key = str(random.randint(0, 10000))
        key_string = "Coba Coba[" + random_key + "]"

        status_sentence.send_keys(key_string)
        submit_btn.click()

        self.assertIn(key_string, chrome.page_source)

    def test_profile_button_can_redirect(self):
        chrome = self.browser
        chrome.get('http://127.0.0.1:8000/')

        time.sleep(4)

        profile_btn = chrome.find_element_by_id("profile-btn")
        profile_btn.click()

        time.sleep(4)

        card_text = chrome.find_element_by_class_name("card-text").text

        self.assertEqual(card_text, '1806141252.')

    ## test for challenge

    def test_style_of_h1(self):
        chrome = self.browser
        chrome.get('http://127.0.0.1:8000/')

        time.sleep(4)

        heading = chrome.find_element_by_tag_name("h1")

        self.assertEqual(
            '"Patrick Hand", cursive',
            heading.value_of_css_property("font-family")
        )

        self.assertEqual(
            "30px",
            heading.value_of_css_property("font-size")
        )

    def test_back_to_status_btn_can_be_used(self):
        chrome = self.browser
        chrome.get('http://127.0.0.1:8000/profile/')

        time.sleep(4)

        back_btn = chrome.find_element_by_xpath("/html/body/div/div[1]/div/a[2]")
        back_btn.click()

        self.assertEqual(
            chrome.current_url,
            'http://127.0.0.1:8000/'
        )

    def test_redirect_to_other_website(self):
        chrome = self.browser
        chrome.get('http://127.0.0.1:8000/profile/')

        time.sleep(4)

        web_btn = chrome.find_element_by_link_text("See My Website")
        href_in_btn = web_btn.get_attribute('href')

        time.sleep(4)

        self.assertEqual(
            href_in_btn,
            'https://jahnsmichael.herokuapp.com/'
        )

    def test_change_theme_index(self):
        chrome = self.browser
        chrome.get('http://127.0.0.1:8000/')

        time.sleep(4)  

        # first click
        theme_button = chrome.find_element_by_id("theme")
        theme_button.click()
        theme_button.click()


        body = chrome.find_element_by_tag_name("body")
        class_of_body = body.get_attribute("class")

        self.assertIn(
            "back_theme",
            class_of_body
        )

        # second click
        theme_button.click()
        class_after_double_click = body.get_attribute("class")

        self.assertNotIn(
            "back_theme",
            class_after_double_click
        )

    def test_change_theme_profile(self):
        chrome = self.browser
        chrome.get('http://127.0.0.1:8000/profile')

        time.sleep(4)

        theme_button = chrome.find_element_by_id("theme")
        theme_button.click()
        theme_button.click()

        body = chrome.find_element_by_tag_name("body")
        class_of_body = body.get_attribute("class")

        self.assertIn(
            "back_theme",
            class_of_body
        )

        theme_button.click()
        class_after_double_click = body.get_attribute("class")

        self.assertNotIn(
            "back_theme",
            class_after_double_click
        )

    def test_accordion(self):
        chrome = self.browser
        chrome.get('http://127.0.0.1:8000/profile')

        time.sleep(4)

        accordion_header = chrome.find_element_by_class_name("accordion-header")
        accordion_content = chrome.find_element_by_class_name("accordion-content")

        display_before_click = accordion_content.get_attribute("style")

        self.assertIn(
            "none",
            display_before_click
        )

        accordion_header.click()
        display_after_click = accordion_content.get_attribute("style")

        self.assertNotIn(
            "none",
            display_after_click
        )





