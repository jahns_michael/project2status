from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect

# Create your views here.

def log_in(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid() :
            user = form.get_user()
            print(user)
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            request.session["username"] = user.username
            return HttpResponseRedirect("/list-laptop/")
    else:
        form = AuthenticationForm()
    return render(request, "login.html", {"form" : form})

def sign_up(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid() :
            user = form.save()
            return HttpResponseRedirect("/accounts/login/")
    else:
        form = UserCreationForm()
    return render(request, "signup.html", {"form" : form})

def log_out(request):
    request.session.flush()
    logout(request)
    return HttpResponseRedirect("/list-laptop/")

