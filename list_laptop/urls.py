from django.urls import path
from .views import index, json_render

urlpatterns = [
    path('', index, name='index'),
    path('json-list/', json_render, name='json_render'),
]