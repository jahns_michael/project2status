from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
import requests
import os
import json
import random

# Create your views here.

def index(request):
    return render(request, 'list.html')

def json_render(request):
    # filename = os.getcwd()
    # filename = filename + "\list_laptop\\static\\notebook.json"
    # file_handle = open(filename, "r")
    # json_list = json.loads(file_handle.read())
    req = requests.get("https://enterkomputer.com/api/product/notebook.json")
    json_list = req.json()
    data = list()

    for i in range(100):
        item = json_list[random.randrange(799, 2016)]
        data.append(item)

    return JsonResponse(data, safe=False)

# def wishlist(request):
#     if request.method == "POST":
#         price = request.POST["totalPriceSend"]
#         request.session["total_price"] = price
#         request.session.modified = True
#         print(request.session["total_price"])
#     return HttpResponseRedirect('/list-laptop/')

