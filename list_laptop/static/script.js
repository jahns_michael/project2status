var req = new XMLHttpRequest();

if (sessionStorage.getItem("totalPrice") != null) {
  $("#total-harga").html("Total harga : Rp " + sessionStorage.getItem("totalPrice"));
  $(".footer").attr("style", "display: block");
  $(".footer").slideDown(500);
}

req.open("GET", "json-list/", true);
req.onload = function() {   
    data = JSON.parse(req.responseText);
    console.log(data);
    currentPage = 1;
    renderHTML(data, currentPage);

    $("#next").click(function(){
      if(currentPage + 1 > 10){
        alert("End of the page")
      } else {
        currentPage++;
        renderHTML(data, currentPage);
      }
    })
    $("#prev").click(function(){
      if(currentPage - 1 < 1){
        alert("End of the page")
      } else {
        currentPage--;
        renderHTML(data, currentPage);
      }
    })

    $("button").click(function(){
      var id = parseInt($(this).attr("id"));
      if ($(this).hasClass("btn-dark")) {
        $(this).removeClass("btn-dark");
        $(this).addClass("btn-light");
        addToWishlist(id, data);
        $(this).html("Remove");
      } else if ($(this).hasClass("btn-light")) {
        $(this).addClass("btn-dark");
        $(this).removeClass("btn-light");
        removeFromWishlist(id, data);
        $(this).html("Add");
      } else if ($(this).hasClass("btn-danger")) {
        sessionStorage.clear();
        $("#total-harga").html("Total harga : Rp " + sessionStorage.getItem("totalPrice"));
        $("button").addClass("btn-dark");
        $("button").removeClass("btn-light");
        $("button").html("Add");
        $(this).removeClass("btn-dark");
        $(this).addClass("btn-danger");
        $(this).html("Clear wishlist")
      }
      console.log("Total harga :" + totalPrice);
    });

}
req.send();

function renderHTML(data, page) {
  for (i = 10*page - 9; i <= 10*page; i++) {

    if (i % 10 != 0){
      var detail = data[i-1].details;
      $("#name" + i%10).html(data[i-1].name + '<br>' + detail);
      $("#price" + i%10).html("Rp " + data[i-1].price);
      $("#button" + i%10 + " > button").attr("id", "" + i);
    }else {
      var detail = data[i-1].details;
      $("#name" + 10).html(data[i-1].name + '<br>' + detail);
      $("#price" + 10).html("Rp " + data[i-1].price);
      $("#button" + 10 + " > button").attr("id", "" + i);
    }
  }
};

$(".lead").click(function() {
  sessionStorage.clear();
});

function addToWishlist(num, data) {
  if (sessionStorage.getItem("totalPrice") == null) {
    sessionStorage.setItem("totalPrice", parseInt(data[num-1].price));
  } else {
    sessionStorage.setItem("totalPrice", parseInt(sessionStorage.getItem("totalPrice")) + parseInt(data[num-1].price));
  }
  $("#total-harga").html("Total harga : Rp " + sessionStorage.getItem("totalPrice"));
  $(".footer").attr("style", "display: block");
  $(".footer").slideDown(500);
};

function removeFromWishlist(num, data){
  if (sessionStorage.getItem("totalPrice") == null) {
    sessionStorage.setItem("totalPrice", parseInt(data[num-1].price));
  } else {
    sessionStorage.setItem("totalPrice", parseInt(sessionStorage.getItem("totalPrice")) - parseInt(data[num-1].price));
  }
  $("#total-harga").html("Total harga : Rp " + sessionStorage.getItem("totalPrice"));
};



