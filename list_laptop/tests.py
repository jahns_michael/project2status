from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, json_render

# Create your tests here.
class ListLaptopTest(TestCase):

    def test_list_laptop_app_url_exist(self):
        response = Client().get('/list-laptop/')
        self.assertEqual(response.status_code, 200)

    def test_list_laptop_app_using_index_func(self):
        found = resolve('/list-laptop/')
        self.assertEqual(found.func, index)

    def test_list_laptop_contains_title_and_table(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        title = "List Laptop"
        num_col_header = "No"
        name_col_header = "Name"
        price_col_header = "Price"
        self.assertIn(title, html_response)
        self.assertIn(name_col_header, html_response)
        self.assertIn(num_col_header, html_response)
        self.assertIn(price_col_header, html_response)

    def test_json_list_url_exist(self):
        response = Client().get('/list-laptop/json-list/')
        self.assertEqual(response.status_code, 200)

    def test_json_list_using_json_render_func(self):
        found = resolve('/list-laptop/json-list/')
        self.assertEqual(found.func, json_render)
    