from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, profile
from .models import Status
from .forms import StatusForm
from datetime import datetime

# Create your tests here.

class StatusAppTest(TestCase):

    def test_status_app_profile_url_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_status_app_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_status_app_profile_template_contains_profile_data(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        image_link = "https://instagram.fcgk18-2.fna.fbcdn.net/vp/5f299085e890f509bdbab6bda45a0ae3/5CF14C4E/t51.2885-15/e35/36136648_258250158271002_552952029765435392_n.jpg?_nc_ht=instagram.fcgk18-2.fna.fbcdn.net&_nc_cat=106"
        npm = "1806141252"
        name = "Jahns Michael"
        self.assertIn(image_link, html_response)
        self.assertIn(npm, html_response)
        self.assertIn(name, html_response)

    def test_status_app_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_status_app_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_status_app_templates_valid(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Halo, apa kabarmu hari ini?', html_response)

    def test_status_app_success_to_create_status_model(self):
        dummy_status = Status.objects.create(
            status_sentence = "Hello World",
            posting_time = datetime.now()
        )
        counting_all_status_created = Status.objects.all().count()
        self.assertEqual(counting_all_status_created, 1)

    def test_status_app_form_is_valid(self):
        form = StatusForm(data={'status_sentence': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status_sentence'],
            ['This field is required.']
        )

    def test_status_app_post_success_and_render_the_result(self):
        test = 'Aku stress karena tugas PPW terlalu banyak, help kak Pewe!!!'
        response_post = Client().post('/', {'status_sentence': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_status_app_post_error_and_render_the_result(self):
        test = 'Aku stress karena tugas PPW terlalu banyak, help kak Pewe!!!'
        response_post = Client().post('/', {'status_sentence': ''})
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_status_app_object_return_posting_time(self):
        test_time = str(datetime.now().strftime("%Y-%m-%d %H:%M"))
        dummy_status = Status.objects.create(
            status_sentence = "Hello World",
        )
        test_str_func = dummy_status.__str__()
        self.assertEqual(test_str_func, test_time)



    
