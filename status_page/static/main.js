$(document).ready(function(){
    
    showPage();

    $("#theme").on("click", theme);
    $("#theme").on("mouseenter", theme);
    $("#theme").on("mouseleave", theme);

    $("#profile-btn").click(function(){
        showPage();
    })
    
    $("#accordion > div").hide();
    $("#accordion > h3").click(function(){
        $("#accordion > div").slideUp(300);
        $(this).attr("data-content", "+")
        if($(this).next().is(":hidden")){
            $(this).attr("data-content", "-")
            $(this).next().slideDown(300);
        }
    })

    $(".accordion-header").hover(function(){
        $(this).toggleClass("header-theme");
    })

    function theme() {
        $("body").toggleClass("back_theme");
        $(".card-body").toggleClass("back_theme");
        $(".card").toggleClass("back_theme");
        $(".btn").toggleClass("btn_theme");
        $(".list-group-item").toggleClass("list_theme");
        $("li").toggleClass("text_theme");
        $("h1").toggleClass("text_theme");
        $(".accordion-header").toggleClass("header-theme");
        $("p").toggleClass("text_theme");
    }

    function showPage() {
        setTimeout(loadingPage, 3000);
    }

    function loadingPage() {
        $(".loader").attr("style", "display: none");
        $(".container").attr("style", "display: block");
        $("body").css("padding","50px");
    }

})

