from django.apps import AppConfig


class StatusPageConfig(AppConfig):
    name = 'status_page'
