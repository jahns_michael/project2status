from django.shortcuts import render
from .forms import StatusForm
from .models import Status
from django.http import HttpResponseRedirect

# Create your views here.

def index(request):
    if request.method == "POST":
        status_form = StatusForm(request.POST)
        if status_form.is_valid():
            status_form.save()
            return HttpResponseRedirect('/')
    else:        
        status_form = StatusForm()

    db = Status.objects.all()
    content = {'form': status_form, 'data': db}
    return render(request, 'index.html', content)

def profile(request):
    return render(request, 'profile.html')