from django.db import models
from datetime import datetime

# Create your models here.

class Status(models.Model):
    status_sentence = models.TextField()
    posting_time = models.DateTimeField(default=datetime.now().strftime("%Y-%m-%d %H:%M"))

    def __str__(self):
        return str(self.posting_time)